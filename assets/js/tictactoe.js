gstruct = [["", "0-1-2|0-3-6|0-4-8"],
    ["", "1-0-2|1-4-7"],
    ["", "2-1-0|2-4-6|2-5-8"],
    ["", "3-0-6|3-4-5"],
    ["", "4-1-7|4-3-5|4-0-8|4-2-6"],
    ["", "5-4-3|5-2-8"],
    ["", "6-7-8|6-3-0|6-4-2"],
    ["", "7-4-1|7-6-8"],
    ["", "8-4-0|8-5-2|8-7-6"]];

var counter = 0;
var vagainst = "friend";

var player_name = [];

$(document).ready(function () {

    $("." + vagainst).css("background", "#ddd");


    $("#seeAll").click(function () {
        location.href = base_url + 'scores/list';
    });

    $("#scoresBack").click(function () {
        location.href = base_url;
    });




    $(".box").click(function () {
        var selectedid = $(this).attr("id");

        if (vagainst == "friend" && player_name["0"] == undefined){
            player_name["X"] = prompt("Please enter the player X name", "");
            player_name["0"] = prompt("Please enter the player 0 name", "");
        };

        if (vagainst == "computer" && player_name["X"] == undefined){
            player_name["X"] = prompt("Please enter your player name", "");
        };

        gamestatus = tictactoeboard.executeStep(selectedid);
        if (gamestatus == "cont" && vagainst == "computer") {
            var offensivevar = tictactoeboard.offensiveMove();
            if (offensivevar != "") {
                moveid = offensivevar;
            } else {
                moveid = tictactoeboard.intelligentMove(selectedid);
            }
            setTimeout(function () {
                tictactoeboard.executeStep(moveid);
            }, 300);
        }
    });


    $(".computer").click(function () {


        if (vagainst == "computer" && player_name["X"] !== undefined) {
            return;
        }

        player_name["X"] = prompt("Please enter your player name", "");

        $(this).css("background", "#ddd");
        $(".friend").css("background", "#ccc");
        vagainst = "computer";
        tictactoeboard.clearBoard();
    });


    $(".friend").click(function () {



        if (vagainst == "friend" && player_name["0"] !== undefined) {
            return;
        }

        player_name["X"] = prompt("Please enter the player X name", "");
        player_name["0"] = prompt("Please enter the player 0 name", "");
        $(this).css("background", "#ddd");
        $(".computer").css("background", "#ccc");
        vagainst = "friend";
        tictactoeboard.clearBoard();
    });

});

tictactoeboard = {
    executeStep: function (selectedid) {
        /*clear board if game is over and reset counter to 0*/
        if (counter === -1) {
            tictactoeboard.clearBoard();
            return;
        }

        var isValue = $("#" + selectedid).html();
        if (!isValue) {
            var setvalue = "";
            if (counter % 2 === 0) {
                var player = "1";
                $("#" + selectedid).html("<i class='fa fa-times' aria-hidden='true'></i>");
                setvalue = "X";
            } else {
                var player = "2";
                $("#" + selectedid).html("<i class='fa fa-circle-o' aria-hidden='true'></i>");
                setvalue = "0";
            }
            tictactoeboard.chgTurn(player);
            var gamestatus = tictactoeboard.gameLogic(selectedid, setvalue);
            if (gamestatus == "stopgame") return gamestatus;
        } else {
            return;
        }
        counter++;
        /*tictactoeboard.fInOutEffect(selectedid);*/

        if (counter == 9) {
            gamestatus = tictactoeboard.stopGame(counter);
        }
        return gamestatus;

    },

    offensiveMove: function () {
        /*check if there is any computer winning move*/
        var offval = "";
        $.each(gstruct, function (index, value) {
            if (value[0] == "0") {
                var intell_arr = value[1].split("|");
                $.each(intell_arr, function (nindex, nvalue) {
                    var tra_arr = nvalue.split("-");

                    if (gstruct[tra_arr[1]][0] == "0" && gstruct[tra_arr[2]][0] == "") {
                        offval = tra_arr[2];
                        return true;
                    }
                    if (gstruct[tra_arr[1]][0] == "" && gstruct[tra_arr[2]][0] == "0") {
                        offval = tra_arr[1];
                        return true;
                    }
                });
            }
        });
        return offval;
    },

    intelligentMove: function (selectedid) {

        availableboxes = new Array();
        v = 0;
        $.each(gstruct, function (index, value) {
            if (value[0] == "") {
                availableboxes[v++] = index;
            }
        });

        var moveid = "";
        /*find intelligent box from available boxes*/
        bestmove = tictactoeboard.getIntersection(availableboxes, selectedid);
        if (bestmove.length > 0) {
            moveid = bestmove[0];
        } else {
            /*find random box if all intelligent available boxes are filled*/
            randomboxid = Math.floor(Math.random() * availableboxes.length);
            moveid = availableboxes[randomboxid];
        }
        return moveid;
    },

    getIntersection: function (availableboxes, selectedid) {
        var intell_arr = gstruct[selectedid][1].split("|");
        var bestmove = Array();
        var i = 0;
        $.each(availableboxes, function (index, value) {

            $.each(intell_arr, function (nindex, nvalue) {
                var tra_arr = nvalue.split("-");
                $.each(tra_arr, function (nnindex, nnvalue) {
                    if (nnindex == 0) return true;
                    if (value == nnvalue) {
                        bestmove[i++] = value;
                    }
                })

            })
        });

        newbestmove = tictactoeboard.complexMove(bestmove, selectedid);
        return newbestmove;
    },

    complexMove: function (bestmove, selectedid) {
        var intell_arr = gstruct[selectedid][1].split("|");
        var specialval = "";
        $.each(intell_arr, function (nindex, nvalue) {
            var tra_arr = nvalue.split("-");

            if (gstruct[tra_arr[1]][0] == "X" && gstruct[tra_arr[2]][0] == "") {
                specialval = tra_arr[2];
                return true;
            }
            if (gstruct[tra_arr[1]][0] == "" && gstruct[tra_arr[2]][0] == "X") {
                specialval = tra_arr[1];
                return true;
            }
        });
        if (specialval.length == 0) {
            return bestmove;
        } else {
            return specialval;
        }
    },

    stopGame: function (player) {
        $("#turn").addClass("bg-info");
        if (player == 9) {            //if counter is complete / 9 then its TIE
            $("#turn").text("TIE!");
            tictactoeboard.saveGame("TIE");
        } else {
            var against = "";
            playername = (player == "X") ? player_name["X"] : (vagainst == "computer") ? "Computer" : player_name["0"];
            against = (player == "X") ? (vagainst == "computer") ? "Computer" : player_name["0"] : player_name["X"];
            $("#turn").text("(" + player + ") WINNER! - " + playername);
            var gameresult = "(" + player + ") " + playername;
            tictactoeboard.saveGame(gameresult, against);
        }
        $("#turn").fadeIn("slow");
        counter = -1;

        $("#turn").animate({
            fontSize: '+120%'
        });

        return "stopgame";
    },
    saveGame: function (gameresult, against) {

        $.ajax({
            method: 'POST',
            url: base_url + 'scores/create',
            data: {
                against: (vagainst == "computer") ? "computer" : against,
                winner: gameresult
            },
            success: function (data) {
                tictactoeboard.renderScores(data);
            }
            ,
            error: function () {

            },
            progress: function (e) {
                $("#scores").html('<div class="text-center"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></div>');
            }
        });
        /*$.post("scores/create",
         {
         against: vagainst,
         winner: gameresult
         },
         tictactoeboard.renderScores
         );*/
    },
    renderScores: function (data) {
        var dataarray = $.parseJSON(data);
        var strcontent = "";
        $.each(dataarray, function (index, value) {
            strcontent += '<div class="row">';
            strcontent += '<div class="col-md-3">' + value.against + '</div>';
            strcontent += '<div class="col-md-3">' + value.winner + '</div>';
            strcontent += '<div class="col-md-6">' + value.created_at + '</div>';
            strcontent += '</div>';
        });
        $("#scores").html(strcontent);
    },
    fInOutEffect: function (selectedid) {
        $("#" + selectedid).fadeOut("slow");
        /*$("#"+selectedid).fadeIn("3000");*/
    },
    chgColor: function (selectedid) {
        $("#" + selectedid).addClass("text-info");
    },
    chgTurn: function (player) {
        var playervalue = (player == 1) ? "0" : "X";
        /*player=(player==1)?"2":"1";*/
        $("#turn").text("(" + playervalue + ") - " + player_name[playervalue] +  " -  Turn");
    },
    clearBoard: function () {
        for (i = 0; i < 9; i++) {
            $("#" + i).text("");
            gstruct[i][0] = "";
            $("#" + i).removeClass("text-info");
        }
        counter = 0;
        $("#turn").text("(X) Turn");
        $("#turn").css("font-size", "14px");
        /*$("#turn").css("color","black");*/
        $("#turn").addClass("text-info");
        $("#turn").removeClass("bg-info");
    },
    gameLogic: function (selectedid, setvalue) {
        gstruct[selectedid][0] = setvalue;
        var vcombinations = gstruct[selectedid][1];
        var lvar = vcombinations.split("|");
        var gamestopstatus = "cont";
        $.each(lvar, function () {
            var indexvalue = this.split("-");
            var a = gstruct[indexvalue[0]][0];
            var b = gstruct[indexvalue[1]][0];
            var c = gstruct[indexvalue[2]][0];
            if (a === b && a === c && b === c) {
                tictactoeboard.chgColor(indexvalue[0]);
                tictactoeboard.chgColor(indexvalue[1]);
                tictactoeboard.chgColor(indexvalue[2]);
                gamestopstatus = tictactoeboard.stopGame(setvalue);
                return;
            }
        });
        return gamestopstatus;
    }
}
