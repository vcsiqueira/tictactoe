<?php

/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 14/02/17
 * Time: 22:30
 */
class MigrateController extends CI_Controller
{

    public function index()
    {
        $this->load->library('migration');

        if ( ! $this->migration->current())
        {
            echo 'Error' . $this->migration->error_string();
        } else {
            echo 'Migrations ran successfully!';
        }
    }

}