<?php

/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 16/02/17
 * Time: 00:07
 */

function sum($a,$b){
    return $a + $b;
}

class TestingController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("unit_test");
        $this->load->model('scoresmodel');
    }

    public function index(){

        $this->unit->run($this->scoresmodel->get_scores(),"is_array","Testing if the method get_scores is returning right");
        $this->unit->run($this->scoresmodel->getAllScores(),"is_array","Testing if the table scores is populated");
        $this->unit->run($this->scoresmodel->set_scores(["against" => 'Elidioni', 'winner' => '(X) Vinicius']), true,"Testing if the table scores is writing correct");



        $this->load->view("testing/tests");
    }

}