<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ScoresController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('scoresmodel');
        $this->load->helper('url_helper');
        $this->load->helper('html');
    }

    public function index()
    {
        $data['scores'] = $this->scoresmodel->get_scores();
        $this->load->view('scores/index', $data);
    }

    public function viewList()
    {
        $data['scores'] = $this->scoresmodel->getAllScores();
        $this->load->view('templates/header', $data);
        $this->load->view('scores/list', $data);
    }

    public function create()
    {
        $data = array(
            'against' => $this->input->post('against'),
            'winner' => $this->input->post('winner')
        );

        $this->scoresmodel->set_scores($data);

        $data['scores'] = $this->scoresmodel->get_scores();

        $this->load->view('scores/index', $data);
    }
}
?>
