<?php
class TictactoeController extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('scoresmodel');
                $this->load->helper('url_helper');
                $this->load->helper('html');
        }


        public function index()
        {                
            
                $data['scores'] = $this->scoresmodel->get_scores();
                $data['title'] = 'TicTacToe - Jogo da Velha';
                $this->load->view('templates/header', $data);
                $this->load->view('tictactoe/index', $data);
                $this->load->view('templates/footer');
        }

        public function view($slug = NULL)
        {
        }
    
        public function create()
        {
        }
}
