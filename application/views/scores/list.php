<div class="panel panel-default">
    <div class="panel-heading text-center">
        <div class="row">
            <div class="col-1 col-md-11"><strong>Scores</strong></div>
            <div class="col-1 col-md-1"><button id="scoresBack" type="button" class="btn btn-primary">Back</button></div>
        </div>
    </div>
        <div class="container-fluid">
            <div class="row scoresheading text-center">
                <div class="col-1 col-md-4">Against</div>
                <div class="col-1 col-md-4">Winner</div>
                <div class="col-1 col-md-4">Date</div>
            </div>
                    <?php foreach ( $scores as $row ): ?>
                    <div class="row text-center">
                        <div class="col-1 col-md-4"><? echo $row['against']; ?></div>
                        <div class="col-1 col-md-4"><? echo $row['winner']; ?></div>
                        <div class="col-1 col-md-4"><? echo ($row["created_at"]); ?></div>
                    </div>
                    <?php endforeach; ?>
        </div>

</div>


