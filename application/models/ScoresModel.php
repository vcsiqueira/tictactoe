<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ScoresModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_scores($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            $limit=5;
            $start_row=0;

            $this->db->order_by("id", "DESC");
            $query = $this->db->get('scores', $limit, $start_row);
            return $query->result_array();
        }

        $query = $this->db->get_where('scores', array('slug' => $slug));
        return $query->row_array();
    }

    public function set_scores($data)
    {

        $this->load->helper('url');

        log_message('info', 'new game scores added in db > data: '.print_r($data, true));
        return $this->db->insert('scores', $data);
    }

    public function getAllScores(){

        $this->db->order_by("id", "DESC");
        $query = $this->db->get('scores');
        return $query->result_array();

    }
}

?>
