<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 14/02/17
 * Time: 22:21
 */


class Migration_Add_score extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'against' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10',
                ),
                'winner' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '32',
                ),
                'created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            )
        );

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('scores');
    }

    public function down()
    {
        $this->dbforge->drop_table('scores');
    }
}
