/*
 Navicat Premium Data Transfer

 Source Server         : tictactoe
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost
 Source Database       : tictactoe_challenge

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 02/16/2017 01:48:40 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1');
COMMIT;

-- ----------------------------
--  Table structure for `scores`
-- ----------------------------
DROP TABLE IF EXISTS `scores`;
CREATE TABLE `scores` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `against` varchar(10) NOT NULL,
  `winner` varchar(32) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `scores`
-- ----------------------------
BEGIN;
INSERT INTO `scores` VALUES ('1', 'computer', '(0) Computer', '2017-02-15 22:43:40'), ('2', 'computer', '(0) Computer', '2017-02-15 22:44:06'), ('3', 'computer', '(X) Player 1', '2017-02-15 22:45:02'), ('4', 'computer', '(0) Computer', '2017-02-15 22:45:21'), ('5', 'computer', '(0) Computer', '2017-02-15 22:45:27'), ('6', 'friend', '(X) Player 1', '2017-02-15 22:50:29'), ('7', 'computer', '(0) Computer', '2017-02-15 22:54:11'), ('8', 'computer', '(0) Computer', '2017-02-15 23:53:55'), ('9', 'computer', '(0) Computer', '2017-02-15 23:54:00'), ('10', 'friend', '(0) Player 2', '2017-02-16 00:27:30'), ('11', 'friend', '(X) Vinicius', '2017-02-16 00:50:51'), ('12', 'Vinicius', '(X) Vinicius', '2017-02-16 00:55:25'), ('13', 'Vinicius', '(X) Vinicius', '2017-02-16 00:57:14'), ('14', 'VIni', '(X) VIni', '2017-02-16 00:59:14'), ('15', '0', '(0) Eli', '2017-02-16 01:00:47'), ('16', 'Vini', '(X) Vini', '2017-02-16 01:01:26'), ('17', 'Eli', '(X) Vinicius', '2017-02-16 01:07:19'), ('18', 'Vinicius', '(0) Elidioni', '2017-02-16 01:10:23'), ('19', 'Elidioni', '(X) Vinicius', '2017-02-16 01:12:58'), ('20', 'computer', '(0) Computer', '2017-02-16 01:13:12'), ('21', 'computer', 'TIE', '2017-02-16 01:15:29'), ('22', 'Eli', '(X) VInicius', '2017-02-16 01:15:56'), ('23', 'Vinicius', '(0) Elidioni', '2017-02-16 01:18:20'), ('24', 'Elidioni', '(X) Vinicius', '2017-02-16 01:32:04'), ('25', 'Elidioni', '(X) Vinicius', '2017-02-16 01:32:04'), ('26', 'Elidioni', '(X) Vinicius', '2017-02-16 01:32:20'), ('27', 'Elidioni', '(X) Vinicius', '2017-02-16 01:34:01'), ('28', 'sdsd', '(X) sdsd', '2017-02-16 01:38:41'), ('29', '', '(X) ', '2017-02-16 01:39:11');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
